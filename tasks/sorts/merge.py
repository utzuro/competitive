"""
You are given two integer arrays nums1 and nums2,
sorted in non-decreasing order,
and two integers m and n, representing the number of elements in nums1 and nums2 respectively.

Merge nums1 and nums2 into a single array sorted in non-decreasing order.

The final sorted array should not be returned by the function,
but instead be stored inside the array nums1.
To accommodate this, nums1 has a length of m + n,
where the first m elements denote the elements that should be merged
and the last n elements are set to 0 and should be ignored. nums2 has a length of n.
"""


# pylint: disable=too-few-public-methods
class Solution():
    """
    The solution is meant to be used in leetcode envorionment.
    """

    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: None Do not return anything, modify nums1 in-place instead.
        """

        # Simple solution
        # nums1[m:] = nums2
        # nums1.sort()

        nums1_index, nums2_index, cursor = m-1, n-1, m+n-1

        # Merge in reverse order
        while nums1_index >= 0 and nums2_index >= 0:
            if nums1[nums1_index] >= nums2[nums2_index]:
                nums1[cursor] = nums1[nums1_index]
                nums1_index -= 1
            else:
                nums1[cursor] = nums2[nums2_index]
                nums2_index -= 1
            cursor -= 1

        # Copy the remaining elements from nums2
        while nums2_index >= 0:
            nums1[cursor] = nums2[nums2_index]
            nums2_index -= 1
            cursor -= 1

if __name__ == '__main__':
    sol = Solution()
    nums1 = [1,2,3,0,0,0]
    nums2 = [2,5,6]
    sol.merge(nums1, 3, nums2, 3)
    print(nums1)
    # Output: [1,2,2,3,5,6]
