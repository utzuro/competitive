package main

import (
	"fmt"
)

func main() {
	main := []int{1, 2, 3, 0, 0, 0}
	sub := []int{2, 5, 6}

	merge(main, 3, sub, 3)
	fmt.Println(main)
}

func merge(nums1 []int, m int, nums2 []int, n int) {
	p1 := m - 1
	p2 := n - 1
	p := m + n - 1

	for p1 >= 0 && p2 >= 0 {
		if nums2[p2] >= nums1[p1] {
			nums1[p] = nums2[p2]
			p2--
		} else {
			nums1[p] = nums1[p1]
			p1--
		}
		p--
	}

	for p2 >= 0 {
		nums1[p] = nums2[p2]
		p2--
		p--
	}
}
