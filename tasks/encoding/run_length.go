package main

import "fmt"

func main() {

	tests := []struct {
		i string
		o string
	}{
		{"", ""},
		{"abcd", "1a1b1c1d"},
		{"aaabcd", "3a1b1c1d"},
		{"aaaabbcddda", "4a2b1c3d1a"},
		{"112232", "invalid character error"},
		{"/(#cc()311&&#$#!|~=~}*{", "invalid character error"},
		{"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "100x"},
		{"1xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", "too long error"},
	}

	for _, test := range tests {
		result := encode(test.i)
		if result != test.o {
			fmt.Printf("Failed with test '%s': want '%s', got '%s' \n", test.i, test.o, result)
			return
		}
		fmt.Printf("Passed: '%s' -- '%s' \n", test.i, test.o)
	}

	fmt.Println("\nSUCCESS!")
}

func encode(s string) string {

	if s == "" {
		return ""
	}

	if len(s) > 100 {
		return "too long error"
	}

	var (
		result  string
		counter int
	)

	prev := rune(s[0])

	for _, c := range s {

		if c < 65 || c > 122 {
			return "invalid character error"
		}

		if c == prev {
			counter++
			continue
		}

		result = fmt.Sprint(result, fmt.Sprint(counter), string(prev))
		prev = c
		counter = 1
	}

	result = fmt.Sprint(result, fmt.Sprint(counter), string(prev))

	return result
}
