package main

import (
	"fmt"
	"testing"
)

func TestMaxProfit(t *testing.T) {

	tests := []struct {
		i []int
		o int
	}{
		{[]int{7, 1, 5, 3, 6, 4}, 5},
		// {[]int{7, 1, 5, 3, 6, 4}, 4},
	}

	for _, test := range tests {
		result := MaxProfitHash(test.i)
		if result != test.o {
			t.Log(fmt.Sprintf("Got %d, wanted %d for %v", result, test.o, test.i))
			t.Fail()
		}
	}

}
