package main

import "fmt"

func MaxProfitBrute(list []int) int {

	ledger := make(map[int]int)

	for buy_i, buy_v := range list {
		var max_profit int

		for i := buy_i + 1; i < len(list); i++ {
			profit := list[i] - buy_v
			if profit > max_profit {
				max_profit = profit
			}
		}

		ledger[buy_i] = max_profit
	}

	fmt.Println(ledger)

	var max int
	for _, profit := range ledger {
		if profit > max {
			max = profit
		}
	}

	return max
}

func MaxProfitSmart(list []int) int {

	var left, right, max, max_left, max_right, profit int
	for buy_i := range list {
		right = buy_i
		profit = list[right] - list[left]

		if profit <= 0 {
			left = right
			continue
		}
		if profit > max {
			max = profit
			max_left = left
			max_right = right
		}
	}

	return list[max_right] - list[max_left]
}
