"""
Given two integer arrays find if there are any common elements.
"""

# pylint: disable=too-few-public-methods
class Solution:
    """
    The solution is meant to be used in leetcode envorionment.
    """

    def count_common(self, left: list[int], right: list[int]) -> int:
        """
        """

        saved = set(left[:])
        count = 0

        for el in right:
            if el in saved:
                count += 1

        return count

if __name__ == "__main__":
    left = [1, 2, 3, 4, 5]
    right = [5, 6, 7, 8, 9]

    sol = Solution()

    print(sol.count_common(left, right))
