package main

import "os"

type FileLogger struct {
	events       chan<- string
	errors       chan<- error
	lastSequence uint64
	file         *os.File
}

func (l *FileLogger) Log(message string) error {
	l.events <- message

	return nil
}

func (l *FileLogger) Error(err error) error {
	l.errors <- err

	return nil
}

func NewFileLogger(path string) (*FileLogger, error) {
	file, err := os.OpenFile(path, os.O_APPEND|os.O_RDWR|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return nil, err
	}

	return &FileLogger{file: file}, nil
}

func (l *FileLogger) Run() {
	events := make(chan string, 16)
	l.events = events

	errors := make(chan error, 1)
	l.errors = errors

	go func() {
		for e := range events {

			l.lastSequence++

			_, err := l.file.WriteString(e)

			if err != nil {
				errors <- err
				return
			}
		}
	}()
}
