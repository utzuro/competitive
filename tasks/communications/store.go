package main

import "errors"

var store = make(map[string]string)

func Put(key string, value string) error {
	store[key] = value

	return nil
}

var ErrorNotFound = errors.New("not found")

func Get(key string) (string, error) {
	value, ok := store[key]
	if !ok {
		return "", ErrorNotFound
	}
	return value, nil
}

func Delete(key string) error {
	delete(store, key)

	return nil
}













