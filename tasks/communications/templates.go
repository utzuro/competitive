package main

import (
	"context"
	"crypto/sha1"
	"fmt"
	"sync"
	"time"
)

type Request struct {
}

type Response struct {
}

type Circuit func(context.Context) (string, error)
type Effector func(context.Context) (string, error)
type myFunction func(ctx context.Context) (string, error)

func main() {

	var f Circuit

	wrapped := Breaker(DebounceFirst(f, time.Second*100), 3)
	response, err := wrapped(context.Background())
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(response)
}

func DebounceFirst(circuit Circuit, d time.Duration) Circuit {
	var threshold = time.Now()
	var result string
	var err error
	var m sync.Mutex

	return func(ctx context.Context) (string, error) {
		m.Lock()

		defer func() {
			threshold = time.Now().Add(d)
			m.Unlock()
		}()

		if time.Since(threshold) > d {
			result, err = circuit(ctx)
			threshold = time.Now()
		}

		return result, err
	}
}

func Breaker(circuit Circuit, failureThreshold uint) Circuit {
	var consecutiveFailures uint = 0
	var lastAttempt = time.Now()
	var m sync.RWMutex

	return func(ctx context.Context) (string, error) {
		m.RLock() // read lock

		d := consecutiveFailures - failureThreshold

		if d >= 0 {
			shouldRetryAt := lastAttempt.Add(time.Second * 2 << d)
			if !time.Now().After(shouldRetryAt) {
				m.RUnlock()
				return "", fmt.Errorf("service is unreachable")
			}
		}

		m.RUnlock()

		response, err := circuit(ctx)

		m.Lock()
		defer m.Unlock()

		lastAttempt = time.Now()

		if err != nil {
			consecutiveFailures++
			return response, err
		}

		consecutiveFailures = 0

		return response, nil

	}

}

func Retry(effector Effector, retries int, delay time.Duration) Effector {
	return func(ctx context.Context) (string, error) {
		for r := 0; ; r++ {
			response, err := effector(ctx)
			if err == nil || r >= retries {
				return response, nil
			}

			// not just sleep, but check for the ctx too
			select {
			case <-time.After(delay):
			case <-ctx.Done():
				return "", ctx.Err()
			}
		}
	}
}

func SimpleThrottle(e Effector, max uint, refill uint, d time.Duration) Effector {
	var tokens = max
	var once sync.Once

	return func(ctx context.Context) (string, error) {

		if ctx.Err() != nil {
			return "", ctx.Err()
		}

		once.Do(func() {
			ticker := time.NewTicker(d)

			go func() {
				defer ticker.Stop()

				for {
					select {
					case <-ctx.Done():
						return

					case <-ticker.C:
						t := tokens + refill
						if t > max {
							t = max
						}
						tokens = t
					}
				}
			}()
		})

		if tokens <= 0 {
			return "", fmt.Errorf("throttled")
		}

		tokens--

		return e(ctx)
	}
}

type Throttled func(ctx context.Context, id string) (bool, string, error)
type bucket struct {
	tokens uint
	time   time.Time
}

func Throttle(e Effector, max uint, refill uint, d time.Duration) Throttled {
	buckets := map[string]*bucket{}

	return func(ctx context.Context, uid string) (bool, string, error) {
		b := buckets[uid]

		if b == nil {
			buckets[uid] = &bucket{tokens: max - 1, time: time.Now()}

			str, err := e(ctx)
			return true, str, err
		}

		refillInterval := uint(time.Since(b.time) / d)
		currentTokens := b.tokens + refill*refillInterval

		if currentTokens < 1 {
			return false, "", nil
		}

		if currentTokens > max {
			b.time = time.Now()
			b.tokens = max - 1
		} else {
			deltaTokens := currentTokens - b.tokens
			deltaRefills := deltaTokens / refill
			deltaTime := time.Duration(deltaRefills) * d

			b.time = b.time.Add(deltaTime)
			b.tokens = currentTokens - 1
		}

		str, err := e(ctx)

		return true, str, err
	}
}

func Stream(ctx context.Context, in <-chan *Request, out chan<- *Response) error {
	request := <-in
	out <- &Response{}
	fmt.Println(request)

	//dctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	//defer cancel()

	return nil
}

func Funnel(sources ...<-chan int) <-chan int {
	dest := make(chan int)

	var wg sync.WaitGroup

	wg.Add(len(sources))

	for _, ch := range sources {
		go func(ch <-chan int) {
			defer wg.Done()

			for v := range ch {
				dest <- v
			}
		}(ch)
	}

	go func() {
		wg.Wait()
		close(dest)
	}()

	return dest
}

func Split(source <-chan int, n int) []<-chan int {
	dests := make([]<-chan int, 0)

	for i := 0; i < n; i++ {
		ch := make(chan int)
		dests = append(dests, ch)

		go func() {
			defer close(ch)

			for val := range source { // All goroutines compete for the source.
				ch <- val
			}
		}()
	}
	return dests
}

type Future interface {
	Result() (string, error)
}

type InnerFuture struct {
	once sync.Once
	wg   sync.WaitGroup

	res   string
	err   error
	resCh <-chan string
	errCh <-chan error
}

func (f *InnerFuture) Result() (string, error) {
	f.once.Do(func() {
		f.wg.Add(1)
		defer f.wg.Done()
		f.res = <-f.resCh
		f.err = <-f.errCh
	})

	f.wg.Wait()

	return f.res, f.err
}

func SlowFunction(ctx context.Context) Future {
	resCh := make(chan string)
	errCh := make(chan error)

	go func() {
		select {
		case <-time.After(time.Second * 2):
			resCh <- "2 seconds passed"
			errCh <- nil
		case <-ctx.Done():
			resCh <- ""
			errCh <- ctx.Err()
		}
	}()
	return &InnerFuture{resCh: resCh, errCh: errCh}
}

var items = struct {
	sync.RWMutex
	m map[string]int
}{m: make(map[string]int)}

func ThreadSafeRead(key string) (int, bool) {
	items.RLock()
	defer items.RUnlock()
	val, ok := items.m[key]
	return val, ok
}

func ThreadSafeWrite(key string, val int) {
	items.Lock()
	defer items.Unlock()
	items.m[key] = val
}

type Shard struct {
	sync.RWMutex
	m map[string]interface{}
}

type ShardedMap []*Shard

func NewShardedMap(n int) ShardedMap {
	shards := make([]*Shard, n)
	for i := range shards {
		shards[i] = &Shard{m: make(map[string]interface{})}
	}
	return shards
}

func (m ShardedMap) getShardIndex(key string) int {
	checksum := sha1.Sum([]byte(key))
	hash := int(checksum[0]) + int(checksum[1])<<8 + int(checksum[2])<<16 + int(checksum[3])<<24
	return hash % len(m)
}

func (m ShardedMap) getShard(key string) *Shard {
	index := m.getShardIndex(key)
	return m[index]
}
