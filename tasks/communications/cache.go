package main

import (
	"fmt"
	lru "github.com/hashicorp/golang-lru/v2"
)

var cache *lru.Cache

func init() {
	cache, _ = lru.NewWithEvict(2,
		func(key interface{}, value interface{}) {
			fmt.Printf("Evicting %v -> %v")
		},
	)
}

func cacheExample() {
	cache.Add("key1", "value1")
	cache.Add("key2", "value2")

	fmt.Println(cache.Get("key1"))

	cache.Add("key3", "value3")

	fmt.Println(cache.Get("key1"))
}
