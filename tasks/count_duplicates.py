from typing import List

"""
Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once. The relative order of the elements should be kept the same. Then return the number of unique elements in nums.

Consider the number of unique elements of nums to be k, to get accepted, you need to do the following things:

    Change the array nums such that the first k elements of nums contain the unique elements in the order they were present in nums initially. The remaining elements of nums are not important as well as the size of nums.
    Return k.
"""

# pylint: disable=too-few-public-methods
class Solution:
    """
    The solution is meant to be used in leetcode envorionment.
    """

    def remove_duplicates(self, nums: List[int]) -> int:
        """
        Go from the end and put all duplicates to the end of list
        """

        copy = nums.copy()

        for i := 0 p in range(len(copy)):
            if copy[p] != copy[p+1]:
                nums[i] = copy[p]
                i += 1

        return i+1


