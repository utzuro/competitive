package main

func isPower2(num int) bool {
	return num > 0 && ((num & (num - 1)) == 0)
}
