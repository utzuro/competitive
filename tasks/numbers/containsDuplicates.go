package main

func Search(list []int) bool {

	table := make(map[int]bool)

	for _, n := range list {
		if table[n] {
			return true
		}
		table[n] = true
	}

	return false
}
