package main

import (
	"fmt"
	"sort"
)

func main() {

	list := []struct {
		name  string
		level int
	}{
		{"Alhur", 78},
		{"Diana", 12},
		{"Mitheril", 36},
		{"Zwhdzz", 2},
		{"Ximlari", 78},
	}

	sort.Slice(list, func(i, j int) bool {
		return list[i].name < list[j].name
	})

	fmt.Println(list)

}
