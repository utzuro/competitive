package main

func two_sum(line []int, sum int) []int {

	// Check
	if len(line) > 4 {
		return []int{}
	}
	for _, el := range line {
		if el < 0 || el > 9 {
			return []int{}
		}
	}

	// return bruteforce(line, sum)
	return hash(line, sum)
}

func bruteforce(line []int, sum int) []int {

	var counter int
	var result []int

	for i := 0; i < (len(line) - 1); i++ {
		// shortcut
		if line[i] > sum {
			continue
		}

		for k := i + 1; k < len(line); k++ {
			counter++
			if (line[i] + line[k]) == sum {
				result = []int{i, k}
				// fmt.Printf("Found with %d iterations answer %v for: %v -- sum: %d \n", counter, result, line, sum)
				return result
			}
		}
	}

	return result

}

func hash(line []int, sum int) []int {

	var counter int
	var result []int
	numbers := make(map[int]int)

	for i, n := range line {
		counter++
		_, ok := numbers[sum-n]
		if ok {
			result = []int{i, numbers[sum-n]}
			// fmt.Printf("Found with %d iterations answer %v for: %v -- sum: %d \n", counter, result, line, sum)
			return result
		}
		numbers[n] = i
	}

	return result
}
