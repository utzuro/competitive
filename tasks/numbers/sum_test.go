package main

import (
	"fmt"
	"testing"
)

type Test struct {
	i []int
	s int
	o []int
}

func check(one []int, two []int) bool {

	if len(one) == 0 && len(two) == 0 {
		return true
	}

	first := make(map[int]int)

	for i, n := range one {
		first[n] = i
	}

	for _, n := range two {
		_, ok := first[n]
		if ok {
			return true
		}
	}
	return false
}

func TestTwoSum(t *testing.T) {

	tests := make([]Test, 0)

	tests = append(tests,
		Test{
			i: []int{2, 7, 1, 5},
			s: 9,
			o: []int{0, 1},
		},
		Test{
			i: []int{3, 7, 3, 6},
			s: 6,
			o: []int{0, 2},
		},
		Test{
			i: []int{2, 1, 5, 3},
			s: 4,
			o: []int{1, 3},
		},
		Test{
			i: []int{9, 9, 1, 2},
			s: 3,
			o: []int{2, 3},
		},
		Test{
			i: []int{2, 1, 5, 3},
			s: 6,
			o: []int{1, 2},
		},
		Test{
			i: []int{6, 3, 1, 9, 5},
			s: 4,
			o: []int{},
		},
		Test{
			i: []int{3, -1, 9, 3},
			s: 9,
			o: []int{},
		},
	)

	for _, test := range tests {
		result := two_sum(test.i, test.s)
		// if !reflect.DeepEqual(result, t.o) {
		if !check(result, test.o) {
			t.Log(fmt.Sprintf("Error with %v -- got %v\n", t, result))
			t.Fail()
		}
	}
}
