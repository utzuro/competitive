package main

import (
	"fmt"
	"testing"
)

func TestSearch(t *testing.T) {

	tests := []struct {
		i []int
		o bool
	}{
		{[]int{1, 2, 2, 3, 4}, true},
		{[]int{1, 2, 7, 3, 4}, false},
		{[]int{0, 0, 0, 0, 0}, true},
		{[]int{0}, false},
		{[]int{}, false},
		{[]int{100, 103, 1002, 1000000}, false},
	}

	for _, test := range tests {

		result := Search(test.i)
		if result != test.o {
			t.Log(fmt.Sprintf("Want %t, got %t for %v", test.o, result, test.i))
			t.Fail()
		}
	}

}
