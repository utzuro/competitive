package main

import (
	"fmt"
	"testing"
)

func TestIsPower2(t *testing.T) {

	tests := []struct {
		i int
		o bool
	}{
		{64, true},
		{63, false},
		{2, true},
		{128, true},
		{54, false},
		{36, false},
		{6, false},
	}

	for _, test := range tests {
		result := isPower2(test.i)
		if result != test.o {
			t.Log(fmt.Sprintf("Got %t, wanted %t for %d", result, test.o, test.i))
			t.Fail()
		}
	}

}
