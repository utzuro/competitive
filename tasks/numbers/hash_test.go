package main

import (
	"fmt"
	"testing"
)

func TestMyHash(t *testing.T) {
	table := NewHashTable()
	fmt.Println(table)

	hash_tests := []struct {
		i interface{}
		o int
	}{
		{3, 3},
		{3.1, 3},
		{"test", 4},
		{"anoth", 10},
		{'t', 8},
	}

	for _, test := range hash_tests {
		result := getHash(test.i)
		if result != test.o {
			t.Log(fmt.Sprintf("Wanted %d, got %d for the %v", test.o, result, test.i))
			t.Fail()
		}
	}

}
