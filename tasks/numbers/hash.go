// Custom Hash-table implementation
// Not finished but explaines the idea
package main

const (
	Length = 12
)

type HashTable struct {
	array [Length]*bucket
}

type bucket struct {
	head *node
}

type node struct {
	value interface{}
	next  *node
}

func (b *bucket) insert(key interface{}) error {
	head := &node{
		value: key,
		next:  b.head,
	}
	b.head = head

	return nil
}

func (b *bucket) search(key interface{}) (interface{}, bool) {

	currentHead := b.head
	for currentHead != nil {
		if currentHead.value == key {
			return key, true
		}
		currentHead = currentHead.next
	}

	return nil, false
}

func NewHashTable() *HashTable {
	result := &HashTable{}
	for i := 0; i < Length; i++ {
		result.array[i] = &bucket{}
	}
	return result
}

func (h *HashTable) Insert(key interface{}) {
	v, ok := h.Search(key)
	if !ok || v != key {
		h.array[getHash(key)].insert(key)
	}
}

func (h *HashTable) Search(key interface{}) (interface{}, bool) {
	index := getHash(key)
	if h.array[index] != nil {
		return h.array[index].search(key)
	}
	return nil, false
}

func getHash(key interface{}) int {
	var index int

	switch key.(type) {
	case int:
		return key.(int) % Length
	case float64:
		return int(key.(float64)) % Length
	case byte:
		return int(key.(byte)) % Length
	case rune:
		return int(key.(rune)) % Length
	case string:
		var result int
		for _, c := range key.(string) {
			result += int(c)
		}
		return result % Length
	}

	return index
}
