class Solution:
    """
    Meant to be run in leetcode environment
    """

    def factorial(self, n :int) -> int:
        """
        Use recursion
        """
        if n <= 2:
            return n
        return n * self.factorial(n-1)



if __name__ == "__main__":
    s = Solution()

    print(s.factorial(5))
