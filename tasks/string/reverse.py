class Solution:
    """
    Meant to be run in leetcode environment
    """

    def reverse(self, line :str) -> str:
        """
        Reverse a string
        """

        # Simple solution
        # return line[::-1]

        output = [""] * len(line)

        for i, c in enumerate(line):
            output[len(line)-1-i] = c


        return "".join(output)


if __name__ == "__main__":
    s = Solution()
    print(s.reverse("Hello World!"))
