from typing import List

"""
Given an integer array nums and an integer val, remove all occurrences of val in nums in-place. 
The order of the elements may be changed. 
Then return the number of elements in nums which are not equal to val.

Consider the number of elements in nums which are not equal to val be k, 
to get accepted, you need to do the following things:

    - Change the array nums such that the first k elements of nums contain the elements which are not equal to val. The remaining elements of nums are not important as well as the size of nums.
    - Return k.
"""

# pylint: disable=too-few-public-methods
class Solution:
    """
    The solution is meant to be used in leetcode envorionment.
    """

    def removeElement(self, nums: List[int], val: int) -> int:
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """

        """
        Logic: move from the back of the list. When you encounter val, replace it with the last element. 
        After replacing, ingore this value (consier the next element to be last from now on)
        val: 2
        1, 2, 1, 1, 2
                    ^t
       2 replaces onto itslef, last is now 3
        1, 1, 1, 1, 2 
                 ^t
        1, 2, 1, 1, 2 
              ^  t
        Just move cursor, keep trash in place for now...
        1, 2, 1, 1, 2 
           ^     t
        Move trash into cursor position and move the trash pointer
        1, 1, 1, 1, 2 
        ^     t   
        Finish

        """

        p:int  = len(nums)-1
        trash: int = len(nums)-1
        while p > 0:
            if nums[p] == val:
                nums[p] = nums[trash]
                trash -= 1
            p -= 1
        return trash+1
